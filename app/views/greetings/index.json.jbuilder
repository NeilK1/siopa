json.array!(@greetings) do |greeting|
  json.extract! greeting, :id
  json.url greeting_url(greeting, format: :json)
end
