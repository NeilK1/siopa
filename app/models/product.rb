class Product < ActiveRecord::Base
     has_attached_file   :image, :styles => { :medium => "200x>", :thumb => "100x100>" }, :default_url => "images.jpg"
     validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    has_many :line_items
    has_many :orders, through: :line_items
    belongs_to :categories
    before_destroy :ensure_not_referenced_by_any_line_item
  
    
    private
    def ensure_not_referenced_by_any_line_item
        if line_items.empty?
        return true
    else
        errors.add(:base, 'Line Items present')
        return false
        end
    end
end
